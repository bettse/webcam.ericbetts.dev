import React from "react";

import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";

import "./App.css";

function App() {

  return (
    <>
      <Navbar collapseOnSelect expand="sm" bg="dark" variant="dark">
        <Navbar.Brand>Webcam</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Item className="px-3">
            </Nav.Item>
          </Nav>
          <Nav>
            &nbsp;
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      <Container className="mt-5">
        <Row className="justify-content-center align-items-center text-center">
          (May take a few seconds to load)
          <video preload="auto" autoPlay muted controls src="https://webcam4.tail829e6.ts.net/api/stream.mp4?src=front_window&mp4=all" style={{display: 'block', width: '100%', height: '100%'}} alt="Livestream via tailscale" />
        </Row>
      </Container>
    </>
  );
}

export default App;
